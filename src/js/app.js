$(document).ready(function(){

  let input_name = $('.js-form__input--name')[0];
  let input_email = $('.js-form__input--email')[0];
  let textarea = $('.js-text-appeal__text')[0];
  let name_warning = $('.js-form__name-warning')[0];
  let email_warning = $('.js-form__email-warning')[0];
  let textarea_warning = $('.js-form__textarea-warning')[0];
  let textarea_is_valid = false;
  let input_name_is_valid = false;
  let input_email_is_valid = false;

//----- Функция проверки валидности заполнения элементов формы
  function formIsValid() {
    if ((textarea_is_valid) && (input_email_is_valid) && (input_name_is_valid)) {
      $('.js-feedback__modal-window').css('display', 'flex')
      return true;
    } else {
      if (!input_name_is_valid) name_warning.classList.add('form__warning--invalid');
      if (!input_email_is_valid) email_warning.classList.add('form__warning--invalid');
      if (!textarea_is_valid) textarea_warning.classList.add('form__warning--invalid');      
      return false;
    }
  };

//----- обработка события отпускания клавиши при вводе ФИО
  $(input_name).on('keyup', function(){
    let value = this.value;
    let check = /[a-zA-Zа-яА-Я]+/.test(value);
    if (!check) {
      name_warning.classList.add('form__warning--invalid');
      input_name_is_valid = false;
    }
    else {
      name_warning.classList.remove('form__warning--invalid');
      input_name_is_valid = true;
    }
  })

//----- обработка события отпускания клавиши при вводе email
  $(input_email).bind('input', function(){
      let value = this.value;
      let check = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i.test(value);
      if (!check) {
        email_warning.classList.add('form__warning--invalid');
        input_email_is_valid = false;
      }
      else {
        email_warning.classList.remove('form__warning--invalid');
        input_email_is_valid = true;
      }
    })

//----- обработка события потери фокуса при вводе текста обращения
    $(textarea).on('blur', function(){
      let value = this.value;
      if (!(/[a-zA-Zа-яА-Я]+/.test(value))) {
        textarea_warning.classList.add('form__warning--invalid');
        textarea_is_valid = false;
      }
      else {
        textarea_warning.classList.remove('form__warning--invalid');
        textarea_is_valid = true;
      }
    })

//----- обработка события при нажатии кнопки отправки формы
$('.js-form__btn-submit').click(function(event) {
  event.preventDefault();
  formIsValid();
});

//----- обработка события при отправки формы
  $('.js-form__holder').on('submit', function(event) {
    if (!formIsValid()) return;
    event.preventDefault();
    let data = JSON.stringify($('.js-form__holder').serializeArray());
    console.log(data);
    $.ajax({
      url: $(this).attr('action'),
      dataType: 'json',
      method: 'get',
      data: data,
      succsess: function(response) {
        console.log('Данные успешно обработаны!');
      },
      error: function(err) {
        console.log('Возникла проблема при отправке формы!');
      }
    })
  })

//----- обработка события закрытия модульного окна отправки формы обратной связи
  $('.js-modal-window__closed').on('click', function() {
    $('.js-feedback__modal-window').css('display', 'none')
  })

//----- плагин selectmenu внешний вид с сохранением доступности
  $('.js-form__select').selectmenu({
    position: { my : 'top', at: 'bottom+10' }
  });

//----- плагин красивого курсора в selectmenu
  $('.ui-menu').niceScroll({
    cursorminheight: "117",
    railpadding: { top: 0, right: 3, left: 0, bottom: 0 },
    cursorcolor: "#C4C4C4",
    cursorwidth: "3px",
    cursoropacitymin: "1",
    cursorborder: "none",
    mousescrollstep: "10"
  });
})